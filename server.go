package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type CORSRouterDecorator struct {
	R *mux.Router
}

// database connection
func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "root"
	//dbPass := ""
	dbName := "tcp(127.0.0.1:3306)/crud-app"
	db, err := sql.Open(dbDriver, dbUser+"@"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db
}

// struct
type Shipment struct {
	ID              int    `json:"id,omitempty"`
	Containernr     string `json:"containernr,omitempty"`
	Mblnumber       string `json:"mblnumber,omitempty"`
	Scac_code       string `json:"scac_code,omitempty"`
	Atd_time        string `json:"atd_time,omitempty"`
	Eta_time        string `json:"eta_time,omitempty"`
	Messagefunction string `json:"messagefunction,omitempty"`
}

// GET
func getShipments(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var shipments []Shipment
	db := dbConn()
	selDB, err := db.Query("SELECT * FROM shipments ORDER BY id DESC")
	if err != nil {
		panic(err.Error())
	}
	for selDB.Next() {
		var shipment Shipment
		err = selDB.Scan(&shipment.ID, &shipment.Containernr, &shipment.Mblnumber, &shipment.Scac_code, &shipment.Atd_time, &shipment.Eta_time, &shipment.Messagefunction)
		if err != nil {
			panic(err.Error())
		}
		shipments = append(shipments, shipment)
	}
	json.NewEncoder(w).Encode(shipments)
	defer db.Close()
}

func getShipment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	var shipments []Shipment
	db := dbConn()
	selDB, err := db.Query("SELECT * FROM shipments WHERE id=?", params["id"])
	if err != nil {
		panic(err.Error())
	}
	for selDB.Next() {
		var shipment Shipment
		err = selDB.Scan(&shipment.ID, &shipment.Containernr, &shipment.Mblnumber, &shipment.Scac_code, &shipment.Atd_time, &shipment.Eta_time, &shipment.Messagefunction)
		if err != nil {
			panic(err.Error())
		}
		shipments = append(shipments, shipment)
	}
	json.NewEncoder(w).Encode(shipments)
	defer db.Close()
}

// POST
func postShipment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var shipment Shipment
	_ = json.NewDecoder(r.Body).Decode(&shipment)
	db := dbConn()
	insForm, err := db.Prepare("INSERT INTO shipments(containernr, mblnumber, scac_code, atd_time, eta_time, messagefunction) VALUES(?,?,?,?,?,?)")
	if err != nil {
		panic(err.Error())
	}
	insForm.Exec(shipment.Containernr, shipment.Mblnumber, shipment.Scac_code, shipment.Atd_time, shipment.Eta_time, shipment.Messagefunction)
	fmt.Fprintf(w, "New Shipment Added Successfully")
	defer db.Close()
}

// PUT
func putShipment(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	db := dbConn()
	selDB, err := db.Query("SELECT * FROM shipments WHERE id=?", params["id"])
	if err != nil {
		panic(err.Error())
	}
	var shipment Shipment
	for selDB.Next() {
		err = selDB.Scan(&shipment.ID, &shipment.Containernr, &shipment.Mblnumber, &shipment.Scac_code, &shipment.Atd_time, &shipment.Eta_time, &shipment.Messagefunction)
		if err != nil {
			panic(err.Error())
		}
	}
	json.NewDecoder(r.Body).Decode(&shipment)
	insForm, err := db.Prepare("UPDATE shipments SET containernr=?, mblnumber=?, scac_code=?, atd_time=?, eta_time=?, messagefunction=? WHERE id=?")
	if err != nil {
		panic(err.Error())
	}
	insForm.Exec(shipment.Containernr, shipment.Mblnumber, shipment.Scac_code, shipment.Atd_time, shipment.Eta_time, shipment.Messagefunction, params["id"])
	fmt.Fprintf(w, "Shipment Updated Successfully")
	defer db.Close()
}

// DELETE
func deleteShipment(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	db := dbConn()
	delForm, err := db.Prepare("DELETE FROM shipments WHERE id=?")
	if err != nil {
		panic(err.Error())
	}
	delForm.Exec(params["id"])
	fmt.Fprintf(w, "Shipment Deleted Successfully")
	defer db.Close()
}

// call external api
func callExternalApi() error {
	//make a call to external API
	return nil
}

// main function
func main() {
	router := mux.NewRouter()

	router.HandleFunc("/shipments", CORS(getShipments)).Methods("GET", "OPTIONS")
	router.HandleFunc("/shipments/{id}", CORS(getShipment)).Methods("GET", "OPTIONS")
	router.HandleFunc("/shipments", CORS(postShipment)).Methods("POST", "OPTIONS")
	router.HandleFunc("/shipments/{id}", CORS(putShipment)).Methods("PUT", "OPTIONS")
	router.HandleFunc("/shipments/{id}", CORS(deleteShipment)).Methods("DELETE", "OPTIONS")

	log.Fatal(http.ListenAndServe(":8000", router))
}

func CORS(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Access-Control-Allow-Credentials", "true")
		w.Header().Add("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")

		if r.Method == "OPTIONS" {
			http.Error(w, "No Content", http.StatusNoContent)
			return
		}

		next(w, r)
	}
}
