module gitlab.com/severinsm/crud-shipments

go 1.19

require (
	github.com/go-sql-driver/mysql v1.7.0
	github.com/gorilla/mux v1.8.0
)
